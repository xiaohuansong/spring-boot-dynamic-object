package com.northking.chainResponsibility.exampleChain;

import com.northking.chainResponsibility.ChainHandlerDetail;
import com.northking.chainResponsibility.ChainHandlerMain;
import com.northking.chainResponsibility.exampleChain.ChainDetails.ExampleBusiness;
import com.northking.chainResponsibility.exampleChain.ChainDetails.ExampleChainDetailADetail;
import com.northking.chainResponsibility.exampleChain.ChainDetails.ExampleChainDetailBDetail;
import com.northking.chainResponsibility.exampleChain.ChainDetails.ExampleChainDetailCDetail;

/**
 * @author BigHead
 * @date 2021/11/1 14:49
 * @description ExampleChainMain
 * 继承责任链 起始点对象
 */

public class ExampleChainHandlerMain extends ChainHandlerMain<ExampleBusiness> {
    public ExampleChainHandlerMain() {
        this
                .doDetailsHandler(new ExampleChainDetailADetail())
                .doDetailsHandler(new ExampleChainDetailBDetail())
                .doDetailsHandler(new ExampleChainDetailCDetail());
    }

    @Override
    public void startChain(ExampleBusiness object) {
        for (ChainHandlerDetail<ExampleBusiness> chainHandlerDetail : handlerList) {
            chainHandlerDetail.putInStorage(object);
        }
    }
}
