package com.northking.chainResponsibility.exampleChain.ChainDetails;

import com.northking.chainResponsibility.ChainHandlerDetail;


/**
 * @author BigHead
 * @date 2021/11/1 14:53
 * @description ExampleChainDetailC
 * 链路规则C
 */

public class ExampleChainDetailCDetail implements ChainHandlerDetail<ExampleBusiness> {

    @Override
    public void putInStorage(ExampleBusiness object) {
        object.setSumValue(object.getJan().add(object.getFeb()));
        System.out.println(object);
    }
}
