package com.northking.chainResponsibility.exampleChain.ChainDetails;

import com.northking.chainResponsibility.ChainHandlerDetail;

import java.math.BigDecimal;

/**
 * @author BigHead
 * @date 2021/11/1 14:53
 * @description ExampleChainDetailB
 * 链路规则B
 */
public class ExampleChainDetailBDetail implements ChainHandlerDetail<ExampleBusiness> {

    @Override
    public void putInStorage(ExampleBusiness object) {
        object.setFeb(new BigDecimal("2.0"));
    }
}
