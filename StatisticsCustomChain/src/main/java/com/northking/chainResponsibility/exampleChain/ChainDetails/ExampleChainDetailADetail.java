package com.northking.chainResponsibility.exampleChain.ChainDetails;

import com.northking.chainResponsibility.ChainHandlerDetail;

import java.math.BigDecimal;

/**
 * @author BigHead
 * @date 2021/11/1 14:53
 * @description ExampleChainDetailA
 * 链路规则A
 */

public class ExampleChainDetailADetail implements ChainHandlerDetail<ExampleBusiness> {

    @Override
    public void putInStorage(ExampleBusiness object) {
        object.setJan(new BigDecimal("1.9"));
    }
}
