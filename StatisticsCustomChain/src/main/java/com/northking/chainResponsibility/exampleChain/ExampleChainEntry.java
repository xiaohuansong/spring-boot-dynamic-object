package com.northking.chainResponsibility.exampleChain;

import com.northking.chainResponsibility.ChainEntityInterface;
import com.northking.chainResponsibility.ChainEntryAnnotation;
import com.northking.chainResponsibility.ChainEntryInterface;
import com.northking.chainResponsibility.exampleChain.ChainDetails.ExampleBusiness;


/**
 * @author BigHead
 * @date 2021/11/1 15:10
 * @description ExampleChainMain
 * 责任链入口函数
 */
@ChainEntryAnnotation(chainEntryClass = ExampleChainEntry.class)
public class ExampleChainEntry implements ChainEntryInterface {
    private final ExampleChainHandlerMain exampleChainMainHandler;

    public ExampleChainEntry(){
        this.exampleChainMainHandler = new ExampleChainHandlerMain();
    }

    @Override
    public void start(ChainEntityInterface chainEntityInterface) {
        exampleChainMainHandler.startChain((ExampleBusiness) chainEntityInterface);
    }
}
