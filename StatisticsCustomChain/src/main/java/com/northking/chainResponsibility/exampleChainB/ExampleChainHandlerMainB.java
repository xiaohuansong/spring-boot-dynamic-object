package com.northking.chainResponsibility.exampleChainB;

import com.northking.chainResponsibility.ChainHandlerDetail;
import com.northking.chainResponsibility.ChainHandlerMain;
import com.northking.chainResponsibility.exampleChainB.ChainDetails.ExampleBusinessB;
import com.northking.chainResponsibility.exampleChainB.ChainDetails.ExampleChainDetailADetailB;
import com.northking.chainResponsibility.exampleChainB.ChainDetails.ExampleChainDetailBDetailB;
import com.northking.chainResponsibility.exampleChainB.ChainDetails.ExampleChainDetailCDetailB;

/**
 * @author BigHead
 * @date 2021/11/1 14:49
 * @description ExampleChainMain
 * 继承责任链 起始点对象
 */

public class ExampleChainHandlerMainB extends ChainHandlerMain<ExampleBusinessB> {
    public ExampleChainHandlerMainB() {
        this
                .doDetailsHandler(new ExampleChainDetailADetailB())
                .doDetailsHandler(new ExampleChainDetailBDetailB())
                .doDetailsHandler(new ExampleChainDetailCDetailB());
    }

    @Override
    public void startChain(ExampleBusinessB object) {
        for (ChainHandlerDetail<ExampleBusinessB> chainHandlerDetail : handlerList) {
            chainHandlerDetail.putInStorage(object);
        }
    }
}
