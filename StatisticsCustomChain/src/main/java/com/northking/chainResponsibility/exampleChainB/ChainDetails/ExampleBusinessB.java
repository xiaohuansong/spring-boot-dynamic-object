package com.northking.chainResponsibility.exampleChainB.ChainDetails;

import com.northking.chainResponsibility.ChainEntityAnnotation;
import com.northking.chainResponsibility.ChainEntityInterface;
import com.northking.chainResponsibility.exampleChainB.ExampleChainEntryB;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author BigHead
 * @date 2021/11/1 14:52
 * @description ExampleBusiness
 * 链路规则对象
 */
@Data
@ChainEntityAnnotation(chainEntityClass = ExampleBusinessB.class, chainEntryClass = ExampleChainEntryB.class)
public class ExampleBusinessB implements ChainEntityInterface {
    private String eventName;
    private Integer runStep;
    private BigDecimal jan;
    private BigDecimal feb;
    private BigDecimal sumValue;

    @Override
    public String toString() {
        return "ExampleBusiness =>[" +
                eventName +
                ",一月=>" + jan +
                ",二月=>" + feb +
                ",汇总=>" + sumValue + "]";
    }
}
