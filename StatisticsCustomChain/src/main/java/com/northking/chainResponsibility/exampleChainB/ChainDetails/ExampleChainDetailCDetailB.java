package com.northking.chainResponsibility.exampleChainB.ChainDetails;

import com.northking.chainResponsibility.ChainHandlerDetail;


/**
 * @author BigHead
 * @date 2021/11/1 14:53
 * @description ExampleChainDetailC
 * 链路规则C
 */

public class ExampleChainDetailCDetailB implements ChainHandlerDetail<ExampleBusinessB> {

    @Override
    public void putInStorage(ExampleBusinessB object) {
        object.setSumValue(object.getJan().add(object.getFeb()));
        System.out.println(object);
    }
}
