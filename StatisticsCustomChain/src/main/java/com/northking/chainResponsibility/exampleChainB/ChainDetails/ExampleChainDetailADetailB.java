package com.northking.chainResponsibility.exampleChainB.ChainDetails;

import com.northking.chainResponsibility.ChainHandlerDetail;

import java.math.BigDecimal;

/**
 * @author BigHead
 * @date 2021/11/1 14:53
 * @description ExampleChainDetailA
 * 链路规则A
 */

public class ExampleChainDetailADetailB implements ChainHandlerDetail<ExampleBusinessB> {

    @Override
    public void putInStorage(ExampleBusinessB object) {
        object.setJan(new BigDecimal("10"));
    }
}
