package com.northking.chainResponsibility.exampleChainB;

import com.northking.chainResponsibility.ChainEntityInterface;
import com.northking.chainResponsibility.ChainEntryAnnotation;
import com.northking.chainResponsibility.ChainEntryInterface;
import com.northking.chainResponsibility.exampleChainB.ChainDetails.ExampleBusinessB;


/**
 * @author BigHead
 * @date 2021/11/1 15:10
 * @description ExampleChainMain
 * 责任链入口函数
 */
@ChainEntryAnnotation(chainEntryClass = ExampleChainEntryB.class)
public class ExampleChainEntryB implements ChainEntryInterface {
    private final ExampleChainHandlerMainB exampleChainMainHandler;

    public ExampleChainEntryB(){
        this.exampleChainMainHandler = new ExampleChainHandlerMainB();
    }

    @Override
    public void start(ChainEntityInterface chainEntityInterface) {
        exampleChainMainHandler.startChain((ExampleBusinessB) chainEntityInterface);
    }
}
