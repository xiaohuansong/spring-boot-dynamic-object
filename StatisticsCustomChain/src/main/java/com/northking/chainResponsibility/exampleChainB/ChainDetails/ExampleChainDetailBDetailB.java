package com.northking.chainResponsibility.exampleChainB.ChainDetails;

import com.northking.chainResponsibility.ChainHandlerDetail;

import java.math.BigDecimal;

/**
 * @author BigHead
 * @date 2021/11/1 14:53
 * @description ExampleChainDetailB
 * 链路规则B
 */
public class ExampleChainDetailBDetailB implements ChainHandlerDetail<ExampleBusinessB> {

    @Override
    public void putInStorage(ExampleBusinessB object) {
        object.setFeb(new BigDecimal("20.1"));
    }
}
