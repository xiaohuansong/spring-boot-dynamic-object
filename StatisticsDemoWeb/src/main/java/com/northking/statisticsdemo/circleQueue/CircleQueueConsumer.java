package com.northking.statisticsdemo.circleQueue;

import cn.hutool.core.io.FileUtil;
import com.lmax.disruptor.WorkHandler;
import com.northking.chainResponsibility.ChainEntryAnnotation;
import com.northking.chainResponsibility.ChainEntryInterface;
import lombok.AllArgsConstructor;
import org.reflections.Reflections;
import org.reflections.util.ConfigurationBuilder;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author BigHead
 * @date 2021/11/1 9:54
 * @description CircleQueueConsumer
 */
@Configuration
@AllArgsConstructor
public class CircleQueueConsumer extends DynamicLoadBusinessJar implements WorkHandler<CircleQueueEvent> {

    private Map<String, ChainEntryInterface> chainEntryMap;

    public CircleQueueConsumer() {
        chainEntryMap = new ConcurrentHashMap<>();
        flushChainEntryMap();
    }

    @Override
    public void onEvent(CircleQueueEvent circleQueueEvent) {
        if (chainEntryMap.containsKey(circleQueueEvent.getChainEntryName())) {
            chainEntryMap.get(circleQueueEvent.getChainEntryName()).start(circleQueueEvent.getChainEntityInterface());
        } else {
            System.out.println("无匹配逻辑");
        }
    }


    /**
     * 动态扫描装载 责任链入口函数
     */
    public void flushChainEntryMap() {
        initJarPath();
        Set<Class<?>> chainEntryClassSet = reflections.getTypesAnnotatedWith(ChainEntryAnnotation.class);
        Map<String, ChainEntryInterface> chainEntryTempMap = new HashMap<>(chainEntryClassSet.size());
        for (Class<?> clazz : chainEntryClassSet) {
            // 扫描 链路入口类
            final ChainEntryAnnotation chainEntryAnnotation = clazz.getAnnotation(ChainEntryAnnotation.class);
            // 生成链路入口类
            Class<? extends ChainEntryInterface> chainEntryInterfaceClass = chainEntryAnnotation.chainEntryClass();
            // 实例化链路入口类 TODO 这里相关mapper没有传入 后续优化
            ChainEntryInterface chainEntryInterface;
            try {
                chainEntryInterface = chainEntryInterfaceClass.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
                // 初始化异常这里应该增加log做输出，而且为了不影响正常使用，直接跳过
                continue;
            }
            chainEntryTempMap.put(
                    chainEntryInterfaceClass.getSimpleName(),
                    chainEntryInterface
            );
            chainEntryMap.put(
                    chainEntryInterfaceClass.getSimpleName(),
                    chainEntryInterface
            );
        }
        // 清理/更新 入口类
        for (String chainEntryName : chainEntryMap.keySet()) {
            if (!chainEntryTempMap.containsKey(chainEntryName)) {
                chainEntryMap.remove(chainEntryName);
            }
        }
    }
}
