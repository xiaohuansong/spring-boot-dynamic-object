package com.northking.statisticsdemo.circleQueue;

import com.northking.chainResponsibility.ChainEntityInterface;
import lombok.Data;

/**
 * @author BigHead
 * @date 2021/11/1 9:50
 * @description CircleQueueEvent
 * 环形队列事件对象
 */
@Data
public class CircleQueueEvent {
    private String chainEntryName;
    private ChainEntityInterface chainEntityInterface;
}
