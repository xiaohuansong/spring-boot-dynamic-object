package com.northking.statisticsdemo.circleQueue;

import com.lmax.disruptor.EventFactory;

/**
 * @author BigHead
 * @date 2021/11/1 10:42
 * @description CircleQueueEventFactory
 */
public class CircleQueueEventFactory implements EventFactory<CircleQueueEvent> {
    @Override
    public CircleQueueEvent newInstance() {
        return new CircleQueueEvent();
    }
}
