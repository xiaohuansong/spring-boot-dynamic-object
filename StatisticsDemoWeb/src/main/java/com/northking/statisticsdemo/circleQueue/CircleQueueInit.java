package com.northking.statisticsdemo.circleQueue;

import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import lombok.Data;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Configuration;

/**
 * @author BigHead
 * @date 2021/11/1 10:43
 * @description CircleQueueInit
 */
@Data
@Configuration
public class CircleQueueInit implements InitializingBean {

    private final CircleQueueConsumer circleQueueConsumer;
    private final int INT_BUFFER_SIZE = 1024;
    private CircleQueueEventFactory circleQueueEventFactory = new CircleQueueEventFactory();

    private final Disruptor<CircleQueueEvent> circleQueueEventDisruptor = new Disruptor<CircleQueueEvent>(
            circleQueueEventFactory,
            INT_BUFFER_SIZE,
            (r) -> {
                Thread t = new Thread(r);
                t.setDaemon(true);
                return t;
            },
            ProducerType.MULTI,
            new BlockingWaitStrategy()
    );

    public CircleQueueInit(CircleQueueConsumer circleQueueConsumer){
        this.circleQueueConsumer = circleQueueConsumer;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        this.circleQueueEventDisruptor.handleEventsWithWorkerPool(
                circleQueueConsumer,
                circleQueueConsumer,
                circleQueueConsumer,
                circleQueueConsumer
        );
        this.circleQueueEventDisruptor.start();
    }
}
