package com.northking.statisticsdemo.mapper.generator;

import com.northking.statisticsdemo.entity.generator.LogicTableA;
import com.northking.statisticsdemo.entity.generator.LogicTableAExample;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface LogicTableAMapper {
    @SelectProvider(type=LogicTableASqlProvider.class, method="countByExample")
    long countByExample(LogicTableAExample example);

    @DeleteProvider(type=LogicTableASqlProvider.class, method="deleteByExample")
    int deleteByExample(LogicTableAExample example);

    @Delete({
        "delete from logic_table_a",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into logic_table_a (key_name, key_value)",
        "values (#{keyName,jdbcType=VARCHAR}, #{keyValue,jdbcType=INTEGER})"
    })
    @SelectKey(statement="Postgresql", keyProperty="id", before=false, resultType=Integer.class)
    int insert(LogicTableA record);

    @InsertProvider(type=LogicTableASqlProvider.class, method="insertSelective")
    @SelectKey(statement="Postgresql", keyProperty="id", before=false, resultType=Integer.class)
    int insertSelective(LogicTableA record);

    @SelectProvider(type=LogicTableASqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="key_name", property="keyName", jdbcType=JdbcType.VARCHAR),
        @Result(column="key_value", property="keyValue", jdbcType=JdbcType.INTEGER)
    })
    List<LogicTableA> selectByExample(LogicTableAExample example);

    @Select({
        "select",
        "id, key_name, key_value",
        "from logic_table_a",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="key_name", property="keyName", jdbcType=JdbcType.VARCHAR),
        @Result(column="key_value", property="keyValue", jdbcType=JdbcType.INTEGER)
    })
    LogicTableA selectByPrimaryKey(Integer id);

    @UpdateProvider(type=LogicTableASqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") LogicTableA record, @Param("example") LogicTableAExample example);

    @UpdateProvider(type=LogicTableASqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") LogicTableA record, @Param("example") LogicTableAExample example);

    @UpdateProvider(type=LogicTableASqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(LogicTableA record);

    @Update({
        "update logic_table_a",
        "set key_name = #{keyName,jdbcType=VARCHAR},",
          "key_value = #{keyValue,jdbcType=INTEGER}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(LogicTableA record);
}