package com.northking.statisticsdemo.controller;

import com.alibaba.fastjson.JSONObject;
import com.northking.statisticsdemo.circleQueue.CircleQueueConsumer;
import com.northking.statisticsdemo.circleQueue.CircleQueueProducer;
import com.northking.statisticsdemo.entity.generator.LogicTableA;
import com.northking.statisticsdemo.entity.generator.LogicTableAExample;
import com.northking.statisticsdemo.mapper.generator.LogicTableAMapper;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author BigHead
 * @date 2021/10/29 16:24
 * @description TestController
 */

@AllArgsConstructor
@RestController
@RequestMapping("/test")
public class TestController {

    private final LogicTableAMapper logicTableAMapper;
    private final CircleQueueProducer circleQueueProducer;
    private final CircleQueueConsumer circleQueueConsumer;

    @RequestMapping("/testMain")
    public String exampleData(){
        System.out.println("abc");
        LogicTableAExample logicTableAExample = new LogicTableAExample();
        logicTableAExample.createCriteria().andIdBetween(1, 100);
        List<LogicTableA> logicTableAList = logicTableAMapper.selectByExample(logicTableAExample);
        for (LogicTableA logicTableA: logicTableAList){
            System.out.println(logicTableA.getKeyName());
        }
        return "OK";
    }

    @RequestMapping("/testCircleQueue")
    public String testCircleQueue(){
        JSONObject entityValue = new JSONObject();
        entityValue.put("eventName", "ok");
        entityValue.put("runStep", 0);
        JSONObject entityJson = new JSONObject();
        entityJson.put("entityName", "ExampleBusiness");
        entityJson.put("entityValue", entityValue);
        circleQueueProducer.productionEvent(entityJson);
        return "OK";
    }

    @RequestMapping("/testCircleQueueB")
    public String testCircleQueueB(){
        JSONObject entityValue = new JSONObject();
        entityValue.put("eventName", "ok");
        entityValue.put("runStep", 0);
        JSONObject entityJson = new JSONObject();
        entityJson.put("entityName", "ExampleBusinessB");
        entityJson.put("entityValue", entityValue);
        circleQueueProducer.productionEvent(entityJson);
        return "OK";
    }

    @RequestMapping("/flush")
    public String flush(){
        circleQueueConsumer.flushChainEntryMap();
        circleQueueProducer.flushChainEntity();
        return "OK";
    }
}
