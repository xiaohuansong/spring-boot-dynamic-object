package com.northking.statisticsdemo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author BigHead
 */
@SpringBootApplication
@MapperScan(value = {
        "com.northking.statisticsdemo.mapper.self",
        "com.northking.statisticsdemo.mapper.generator",
})
public class StatisticsDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(StatisticsDemoApplication.class, args);
    }

}
