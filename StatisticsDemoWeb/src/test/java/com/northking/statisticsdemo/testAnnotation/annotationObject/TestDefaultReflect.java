package com.northking.statisticsdemo.testAnnotation.annotationObject;

/**
 * @author BigHead
 * @date 2021/11/3 15:04
 * @description TestDefaultReflect
 */
@ReflectClass
public class TestDefaultReflect {
    public void pName(String name) {
        System.out.println("TestDefaultReflect => annotationName:" + name);
    }
}
