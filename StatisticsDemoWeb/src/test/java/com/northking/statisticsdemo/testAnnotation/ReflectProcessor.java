package com.northking.statisticsdemo.testAnnotation;


import java.lang.reflect.Method;
/**
 * @author BigHead
 * @date 2021/11/3 14:57
 * @description ReflectProcessor
 */
public class ReflectProcessor {

    public void parseMethod(final Class<?> clazz) throws Exception {
        final Object obj = clazz.getConstructor(new Class[] {}).newInstance(new Object[] {});
        final Method[] methods = clazz.getDeclaredMethods();
        for (final Method method : methods) {
            final Reflect my = method.getAnnotation(Reflect.class);
            if (null != my) {
                method.invoke(obj, my.name());
            }
        }
    }
}