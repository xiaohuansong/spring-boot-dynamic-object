package com.northking.statisticsdemo.testAnnotation.annotationObject;

/**
 * @author BigHead
 * @date 2021/11/3 15:22
 * @description TestReflectClassMain
 */
public class TestReflectClassMain {
    public static void main(String[] args) throws Exception{
        ReflectClassProcessor reflectClassProcessor = new ReflectClassProcessor();
        for (int i = 0; i < 100; i++) {
            System.out.println("---------------------START TIMES " + i + "-------------------------------------");
            reflectClassProcessor.getAnnotationClass("com.northking.statisticsdemo.testAnnotation");
            Thread.sleep(2000);
            System.out.println("---------------------END TIMES " + i + "-------------------------------------");
        }
    }
}
