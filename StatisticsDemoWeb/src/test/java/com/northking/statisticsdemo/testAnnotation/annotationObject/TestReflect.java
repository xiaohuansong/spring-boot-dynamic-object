package com.northking.statisticsdemo.testAnnotation.annotationObject;

/**
 * @author BigHead
 * @date 2021/11/3 15:04
 * @description TestReflect
 */
@ReflectClass(name = "TestReflect")
public class TestReflect {

    public void pName(String name) {
        System.out.println("TestReflect => annotationName:" + name);
    }
}
