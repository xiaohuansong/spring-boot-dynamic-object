package com.northking.statisticsdemo.testAnnotation.annotationObject;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author BigHead
 * @date 2021/11/3 15:02
 * @description ReflectClass
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ReflectClass {
    String name() default "ReflectClass";
}
