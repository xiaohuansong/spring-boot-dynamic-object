package com.northking.statisticsdemo.testAnnotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author BigHead
 * @date 2021/11/3 14:57
 * @description Reflect
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Reflect {

    String name() default "sunguoli";

}
