package com.northking.statisticsdemo.testAnnotation.annotationObject;

import org.reflections.Reflections;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Set;

/**
 * @author BigHead
 * @date 2021/11/3 15:16
 * @description ReflectClassProcessor
 */
public class ReflectClassProcessor {
    public void getAnnotationClass(String packagesPath){
        //通过注解扫描指定的包
        Reflections reflections = new Reflections(packagesPath);
        //如果该包下面有被 ReflectClass 注解修饰的类，那么将该类的实例加入到列表中，并最终返回
        Set<Class<?>> objectSet = reflections.getTypesAnnotatedWith(ReflectClass.class);
        for (Class o: objectSet){
            try {
                final ReflectClass reflectClass = (ReflectClass)o.getAnnotation(ReflectClass.class);
                Object oo = o.newInstance();
                Method pNameMethod = o.getDeclaredMethod("pName", String.class);
                pNameMethod.invoke(oo, reflectClass.name());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
