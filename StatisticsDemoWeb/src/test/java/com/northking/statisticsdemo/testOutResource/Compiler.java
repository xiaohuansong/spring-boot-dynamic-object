package com.northking.statisticsdemo.testOutResource;



import java.net.URL;
import java.net.URLClassLoader;


/**
 * @author BigHead
 * @date 2021/11/4 14:01
 * @description BuildJavaFile
 */
public class Compiler {

    public static void main(String[] args) throws Exception {
        ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();

        // This URL for a directory will be searched *recursively*
        URL[] urls = new URL[] { new URL("file:///D:\\javaProject\\StatisticsDemo\\target\\StatisticsDemo-0.0.1-SNAPSHOT.jar")};
        URLClassLoader loader = new URLClassLoader(urls);
        Class<?> clazz = loader.loadClass("com.example.statisticscustomchain.exampleChain.ExampleChainEntry");
        System.out.println(clazz);

    }

}
