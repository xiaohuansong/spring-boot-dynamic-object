package com.northking.statisticsdemo.testRunTimeClass;

/**
 * @author BigHead
 * @date 2021/11/3 17:20
 * @description PointObjectInterface
 */
public interface PointObjectInterface {
    void pName();
}
