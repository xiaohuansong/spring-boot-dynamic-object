package com.northking.statisticsdemo.testRunTimeClass;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.reflections.Reflections;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author BigHead
 * @date 2021/11/3 17:10
 * @description TestRunTimeClassMain
 */
public class TestRunTimeClassMain {
    public static void main(String[] args) throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("keyName", "a");
        jsonObject.put("keyValue", "av");


        Reflections reflections = new Reflections("com.northking.statisticsdemo.testRunTimeClass");
        Set<Class<?>> objectSet = reflections.getTypesAnnotatedWith(PointObjectAnnotation.class);
        Map<String, Class<? extends PointObjectInterface>> objectMap = new HashMap<>();
        for (Class<?> o : objectSet) {
            final PointObjectAnnotation reflectClass = o.getAnnotation(PointObjectAnnotation.class);
//            Class<? extends PointObjectInterface> c = o.asSubclass(reflectClass.targetClass());
//            PointObjectInterface pointObjectInterface = c.newInstance();
//            pointObjectInterface.pName();

            objectMap.put(reflectClass.targetClass().getSimpleName(), reflectClass.targetClass());
        }
        PointObjectInterface pointObjectInterface = null;
        if (objectMap.containsKey("TestObjectA")){
            pointObjectInterface = JSON.parseObject(jsonObject.toString(), objectMap.get("TestObjectA"));
//            Class<? extends PointObjectInterface> clazz = objectMap.get("TestObjectA");
//            pointObjectInterface = clazz.newInstance();
//            for (Field field: clazz.getDeclaredFields()){
//                field.setAccessible(true);
//                if (jsonObject.containsKey(field.getName())){
//                    field.set(pointObjectInterface, jsonObject.get(field.getName()));
//                }
//            }
        }
        System.out.println(pointObjectInterface);
    }
}
