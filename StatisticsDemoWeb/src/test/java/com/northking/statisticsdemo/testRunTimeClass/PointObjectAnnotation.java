package com.northking.statisticsdemo.testRunTimeClass;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author BigHead
 * @date 2021/11/3 17:17
 * @description PointObjectAnnotation
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface PointObjectAnnotation {
    Class<? extends PointObjectInterface> targetClass();
}
