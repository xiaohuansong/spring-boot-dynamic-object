package com.northking.statisticsdemo.testRunTimeClass;

import lombok.Data;

/**
 * @author BigHead
 * @date 2021/11/3 17:18
 * @description TestObjectA
 */
@PointObjectAnnotation(targetClass = TestObjectA.class)
@Data
public class TestObjectA implements PointObjectInterface {

    private String keyName;
    private String keyValue;

    @Override
    public void pName() {
        System.out.println(this.getClass().getSimpleName());
    }
}
