package com.northking.statisticsdemo.testObject;

import lombok.Data;

/**
 * @author BigHead
 * @date 2021/11/5 16:49
 * @description TestObjectEntity
 */
@Data
public class TestObjectEntity implements TestObjectInterface {
    private String a;
    private Integer b;
}
