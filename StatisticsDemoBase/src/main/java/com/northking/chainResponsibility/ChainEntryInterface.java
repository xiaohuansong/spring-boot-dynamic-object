package com.northking.chainResponsibility;

/**
 * @author BigHead
 * @date 2021/11/1 15:19
 * @description ChainEntry
 * 这里改造成注解的方式， 就不用在环形队列中手动注入对象了， 先这么写
 */
public interface ChainEntryInterface {

    /**
     * 责任链入口接口
     * @param chainEntityInterface 抽象实体
     */
    void start(ChainEntityInterface chainEntityInterface);
}
