package com.northking.chainResponsibility;

import java.util.ArrayList;
import java.util.List;

/**
 * @author BigHead
 * @date 2021/11/1 14:19
 * @description ChainHandler
 * 责任链管理对象，可以继承这个抽象类，然后在添加一下自定义的方法等
 */
public abstract class ChainHandlerMain<T extends ChainEntityInterface> {

    protected List<ChainHandlerDetail<T>> handlerList = new ArrayList<>();

    /**
     * 装载责任链
     *
     * @param chainHandlerDetail 详细责任规则
     * @return 责任链路
     */
    public ChainHandlerMain<T> doDetailsHandler(ChainHandlerDetail<T> chainHandlerDetail) {
        handlerList.add(chainHandlerDetail);
        return this;
    }


    /**
     * 责任顺序执行
     *
     * @param object 规则业务对象泛型
     */
    public abstract void  startChain(T object);
}
