package com.northking.chainResponsibility;

/**
 * @author BigHead
 * @date 2021/11/1 14:21
 * @description ChainDetailsHandler
 * 真正责任业务继承的对象接口泛型
 */
public interface ChainHandlerDetail<T extends ChainEntityInterface> {
    /**
     * 规则连起始装载逻辑
     *
     * @param object 规则业务对象泛型
     */
    void putInStorage(T object);
}
