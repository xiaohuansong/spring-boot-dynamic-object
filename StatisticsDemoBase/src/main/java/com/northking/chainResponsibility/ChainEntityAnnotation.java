package com.northking.chainResponsibility;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author BigHead
 * @date 2021/11/3 19:02
 * @description ChainEntityAnnotation
 *
 * 责任链实体注解
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ChainEntityAnnotation {
    Class<? extends ChainEntryInterface> chainEntryClass();
    Class<? extends ChainEntityInterface> chainEntityClass();
}
