package com.northking.chainResponsibility;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author BigHead
 * @date 2021/11/3 19:10
 * @description ChainEntryAnnotation
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ChainEntryAnnotation {
    Class<? extends ChainEntryInterface> chainEntryClass();
}
